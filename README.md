|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| **Twitter** | Alternative | Description | Features | Source Code |
|     | [Nitter](https://nitter.net/) | Nitter is a free and open source alternative Twitter front-end focused on privacy. | \- No JavaScript or ads<br>\- All requests go through the backend, client never talks to Twitter<br>\- Prevents Twitter from tracking your IP or JavaScript fingerprint<br>\- Unofficial API (no rate limits or developer account required)<br>\- Lightweight ( 60KB vs 784KB from [twitter.com](http://twitter.com))<br>\- RSS feeds<br>\- Themes<br>\- Mobile support (responsive design)<br>\- AGPLv3 licensed, no proprietary instances permitted | [GitHub](https://github.com/zedeus/nitter) |
|     | [Mastodon](https://mastodon.social/invite/4GuwqhzK) | Mastodon is a free, open-source social network server based on ActivityPub where<br>users can follow friends and discover new ones. On Mastodon, users can publish anything<br>they want: links, pictures, text, video. All Mastodon servers are interoperable as a federated<br>network |     | [GitHub](https://github.com/tootsuite/mastodon) |
|     | [Yotter](https://github.com/ytorg/yotter) | Allows you to follow and gather all the content from your favorite Twitter and YouTube accounts<br>in a *beautiful* feed so you can stay up to date without compromising your privacy at all. |     | [GitHub](https://github.com/ytorg/yotter) |
|     | [Misskey](https://misskey.io/) | a decentralized microblogging platform born on Earth. Since it exists within the Fediverse,<br>it is mutually linked with other social media platforms. |     | [GitHub](https://github.com/syuilo/misskey) |
|     | [Pleorma](https://pleroma.social/) | Pleroma is a federated social networking platform, compatible with Mastodon and other ActivityPub implementations.<br>It is free software licensed under the AGPLv3. It actually consists of two components: a backend, named simply Pleroma,<br>and a user-facing frontend, named Pleroma-FE.<br>It also includes the Mastodon frontend, if that's your thing. It's part of what we call the fediverse, a federated network of<br>instances which speak common protocols and can communicate with each other. One account on an instance is enough to talk to the entire fediverse! | \## Pleroma-FE<br>Pleroma-FE is the single column user interface bundled with Pleroma.<br>\- Detailed theme system<br>\- NSFW attachment hiding<br>\- Keyword filtering<br>\- Your own custom emoji!<br>\- Standalone, can be run in parallel with other interfaces<br>\- The best and latest in conversation view technology<br>\## Pleroma<br>\- High-performance<br>\- Light memory usage<br>\- Simple to deploy<br>\- Runs on a Raspberry Pi<br>\- Speaks ActivityPub<br>\- Provides an extended Mastodon client API | [GitLab](https://git.pleroma.social/pleroma/pleroma) |

<br><br>

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| **Facebook** | Alternative | Description | Features | Source Code |
|     | [Friendica](https://friendi.ca/) | A decentralized Facebook-style social network | [Features](https://friendi.ca/about/features/) | [GitHub](https://github.com/friendica/friendica) |
|     | [Diaspora](https://diasporafoundation.org/) | A decentralized Facebook alternative focused on freedom and privacy. |     | [GitHub](https://github.com/diaspora/) |
|     | [Minds](https://www.minds.com/register?referrer=funkyspacemonkey) | A free & open source social network dedicated to privacy and freedom |     | [GitLab](https://gitlab.com/minds) |

<br><br>

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| **Instagram** | Alternative | Description | Features | Source Code |
|     | [Bibliogram](https://bibliogram.art/) | Bibliogram is a website that takes data from Instagram’s public profile views and puts it into<br>a friendlier page that loads faster, gives downloadable images, eliminates ads, generates RSS<br>feeds, and doesn’t urge you to sign up. |     | [Sourcehut](https://sr.ht/~cadence/bibliogram/) |
|     | [Pixelfed](https://pixelfed.social/FSM) | Pixelfed is an image sharing platform, an ethical alternative to centralized platforms like<br>Instagram. |     | [GitHub](https://github.com/pixelfed) |

<br><br>

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| **Reddit** | Alternative | Description | Features | Source Code |
|     | [Teddit](https://teddit.net/) | Teddit is a free and open source alternative Reddit front-end focused on privacy. | \- No JavaScript or ads<br>\- All requests go through the backend, client never talks to Reddit<br>\- Prevents Reddit from tracking your IP or JavaScript fingerprint<br>\- Lightweight (teddit frontpage: ~30 HTTP requests with ~270 KB of data downloaded vs. Reddit frontpage: ~190 HTTP requests with ~24 MB) | [Codeberg](https://codeberg.org/teddit/teddit) |
|     | [Lemmy](https://lemmy.ml/) | Lemmy allows you to subscribe to forums you’re interested in, post links and discussions, then vote,<br>and comment on them. Behind the scenes, it is very different; anyone can easily run a server, and all<br>these servers are federated (think email), and connected to the same universe, called the Fediverse. | \- Open source, [AGPL License](https://github.com/LemmyNet/lemmy/blob/main/LICENSE).<br>\- Self hostable, easy to deploy.<br>\- Comes with [Docker](https://lemmy.ml/docs/administration_install_docker.html) and [Ansible](https://lemmy.ml/docs/administration_install_ansible.html).<br>\- Clean, mobile-friendly interface.<br>\- Only a minimum of a username and password is required to sign up!<br>\- User avatar support.<br>\- Live-updating Comment threads.<br>\- Full vote scores `(+/-)` like old Reddit.<br>\- Themes, including light, dark, and solarized.<br>\- Emojis with autocomplete support. Start typing `:`<br>\- User tagging using `@`, Community tagging using `!`.<br>\- Integrated image uploading in both posts and comments.<br>\- A post can consist of a title and any combination of self text, a URL, or nothing else.<br>\- Notifications, on comment replies and when you're tagged.<br>\- Notifications can be sent via email.<br>\- Private messaging support.<br>\- i18n / internationalization support.<br>\- RSS / Atom feeds for `All`, `Subscribed`, `Inbox`, `User`, and `Community`.<br>\- Cross-posting support.<br>\- A *similar post search* when creating new posts. Great for question / answer communities.<br>\- Moderation abilities.<br>\- Public Moderation Logs.<br>\- Can sticky posts to the top of communities.<br>\- Both site admins, and community moderators, who can appoint other moderators.<br>\- Can lock, remove, and restore posts and comments.<br>\- Can ban and unban users from communities and the site.<br>\- Can transfer site and communities to others.<br>\- Can fully erase your data, replacing all posts and comments.<br>\- NSFW post / community support.<br>\- OEmbed support via Iframely.<br>\- High performance.<br>\- Server is written in rust.<br>\- Front end is `~80kB` gzipped.<br>\- Supports arm64 / Raspberry Pi. | [GitHub](https://github.com/LemmyNet) |
|     | [Lobste.rs](https://lobste.rs/) | Reddit alternative. Lemmy allows you to subscribe to forums you’re interested in, post links and discussions,<br>then vote, and comment on them. Behind the scenes, it is very different; anyone can easily run a server, and<br>all these servers are federated (think email), and connected to the same universe, called the Fediverse. |     | [GitHub](https://github.com/lobsters/lobsters) |
|     | [Raddle](https://raddle.me/) | It’s been said Raddle is a community for outsiders, malcontents and wayward dreamers.<br>Tor link: http://lfbg75wjgi4nzdio.onion/ |     | [GitLab](https://gitlab.com/postmill/Postmill) |
|     | [Aether](https://getaether.net/) | Peer-to-peer ephemeral public communities. Open source, self-governing communities with auditable<br>moderation and mod elections |     | [GitHub](https://github.com/nehbit/aether) |
|     | [Ruqqus](https://ruqqus.com/) | Open source Reddit Alternative |     | [GitHub](https://github.com/ruqqus/ruqqus) |
|     | [SaidIt](https://saidit.net/) | Open Source Reddit clone |     | [GitHub](https://github.com/libertysoft3/saidit) |
|     | [Libreddit](https://libredd.it/) | An alternative private front-end to Reddit | \- Fast: written in Rust for blazing fast speeds and safety<br>\- Light: no JavaScript, no ads, no tracking<br>\- Private: all requests are proxied through the server, including media<br>\- Secure: strong [Content Security Policy](https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP) prevents browser requests to Reddit | [GitHub](https://github.com/spikecodes/libreddit) |
|     | [littr.me](https://littr.me/) | This project represents a new attempt at the social link aggregator service. It is modelled after (old)Reddit, HackerNews, and Lobste.rs.<br>It tries to combine the good parts of these services while mapping them on the foundation of an ActivityPub generic service. |     | [GitHub](https://github.com/mariusor/go-littr) |
|     | [Tildes](https://tildes.net/) | Self-hostable |     | [GitLab](https://gitlab.com/tildes/tildes) |
|     | [Aether](https://getaether.net/) | Peer-to-peer ephemeral public communities | \- Ephemeral - It keeps 6 months of posts by default. It's gone after. If something is worth keeping, someone will save it within six months — but not from beyond that. If you screw up, argue for the wrong opinion, and then think otherwise, that's okay. No one is going to come after you — it gives you the freedom to be wrong, and move on. <br>\- Privacy-sensitive - It's peer-to-peer, and it has no servers. It's privacy sensitive because of that, source IP of any specific public post cannot (easily) be determined. Aether is private by default, so that you can choose to be fully private, or fully public yourself. Most people use pseudonyms, though you can use your real name, or company. <br>\- Transparent - Actions of moderators are visible to users. No content can just 'disappear', if something gets deleted, you'll know who did it, why they did it, and if you want, how to get it back. Everyone watches the watchmen. Moderation is important for healthy communities, and Aether adds onto it some checks-and-balances. <br>\- Democratic - Communities can elect and impeach their own mods by voting. If a mod behaves inappropriately, users can disable that mod locally as well. Doing so reverts all changes by that moderator for the user, and counts for one impeachment vote. | [GitHub](https://github.com/nehbit/aether) |
|     | [Akasha](https://beta.akasha.world/) | Decentralized Social Media Platform powered by Ethereum and IPFS |     | [GitHub](https://github.com/AKASHAorg) |
|     | [notabug.io](https://notabug.io/) | Decentralized content aggregator |     | [GitHub](https://github.com/notabugio) |

<br><br>

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| **YouTube** | Alternative | Description | Features | Source Code |
|     | [Invidious](https://invidio.us/) | Invidious is an alternative front-end to YouTube | \- Copylefted libre software (AGPLv3+ licensed)<br>\- Audio-only mode (and no need to keep window open on mobile)<br>\- Lightweight (the homepage is ~4 KB compressed)<br>\- Tools for managing subscriptions:<br>\- Only show unseen videos<br>\- Only show latest (or latest unseen) video from each channel<br>\- Delivers notifications from all subscribed channels<br>\- Automatically redirect homepage to feed<br>\- Import subscriptions from YouTube<br>\- Dark mode<br>\- Embed support<br>\- Set default player options (speed, quality, autoplay, loop)<br>\- Support for Reddit comments in place of YouTube comments<br>\- Import/Export subscriptions, watch history, preferences<br>\- Developer API<br>\- Does not use any of the official YouTube APIs<br>\- Does not require JavaScript to play videos<br>\- No need to create a Google account to save subscriptions<br>\- No ads<br>\- No CoC<br>\- No CLA<br>\- Multilingual (translated into many languages) | [GitHub](https://github.com/iv-org/invidious) |
|     | [FreeTube](https://freetubeapp.io/) | FreeTube is an open source desktop YouTube player built with privacy in mind. Use YouTube without<br>advertisements and prevent Google from tracking you with their cookies and JavaScript. Available for<br>Windows, Mac & Linux thanks to Electron. |     | [GitHub](https://github.com/FreeTubeApp/FreeTube) |
|     | [LBRY](https://lbry.tv/$/invite/@FSM:3) | Alternative to YouTube. LBRY is a place where users can find great videos, music, ebooks, and more: imagine a vast<br>digital library that is available on all of your devices. But under the hood, LBRY is many components working together. |     | [GitHub](https://github.com/lbryio) |
|     | [Odysee](https://odysee.com/$/invite/@FSM:3) | Video platform backed by the creators of LBRY and uses the LBRY blockchain protocol |     | [GitHub](https://github.com/lbryio) |
|     | [Yotter](https://github.com/ytorg/yotter) | Allows you to follow and gather all the content from your favorite Twitter and YouTube accounts in a *beautiful* feed so you<br>can stay up to date without compromising your privacy at all. |     | [GitHub](https://github.com/ytorg/yotter) |
|     | [PeerTube](https://joinpeertube.org/) | PeerTube is a free, decentralized and federated video platform developed as an alternative to other platforms that centralize our<br>data and attention, such as YouTube, Dailymotion or Vimeo. |     | [GitHub](https://github.com/Chocobozzz/PeerTube) |
|     | [Piped](https://piped.kavin.rocks/) | An alternative YouTube frontend which is efficient by design. | ## Features:<br><br>- No Ads<br>- No Tracking<br>- Infinite Scrolling<br>- Integration with SponsorBlock<br>- Integration with LBRY<br>- 4K support<br>- No connections to Google's servers<br>- Playing just audio<br>- Comments<br>- Playlist support<br>- Captions support<br>- Search Suggestions<br>- Livestreams support with a quality selector<br>- PWA support<br>- Support for IOS<br>- Preferences saved locally<br>- Multi-region loadbalancing<br>- Performant by design, designed to handle 1000s of users concurrently | [GitHub](https://github.com/TeamPiped/Piped) |

<br><br>

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| **LinkedIn** | Alternative | Description | Features | Source Code |
|     | [Vutuv](https://github.com/vutuv/vutuv/) | a business network which is hosted at https://www.vutuv.de. Think of it as a free, fast and secure open-source alternative for LinkedIn or XING |     | [GitHub](https://github.com/vutuv/vutuv/) |

<br><br>

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| **Github** | Alternative | Description | Features | Source Code |
|     | [Radicle](https://radicle.xyz/) | open-source alternative to GitHub. Peer-to-peer, removes the dependency for a centralized server. | \- Ability to add multiple remote peers<br>\- Manage multiple peers<br>\- Feature to follow a project from a specific peer<br>\- Share your project using a unique ID<br>\- Does not depend on central servers<br>\- No censorship<br>\- One network interconnected with peers<br>\- Ability to work offline<br>\- Local issues & patches<br>\- Built on Git to make it easy and comfortable for most developers<br>\- Your infrastructure<br>\- Ability to receive funding from your supporters (Ethereum)<br>\- Manage codebases together | [GitHub](https://github.com/vutuv/vutuv/) |

<br><br>

|     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- |
| Google/Bing/Yahoo | Alternative | Country of Origin | Privacy Policy | Tor | Source Code |
|     | [DuckDuckGo](https://duckduckgo.com/) | US 🇺🇸 | [Yes](https://duckduckgo.com/privacy) | [Yes](http://3g2upl4pq6kufc4m.onion/) | [GitHub](https://github.com/duckduckgo) |
|     | [StartPage](https://www.startpage.com/) | Netherlands 🇳🇱 | [Yes](https://www.startpage.com/en/privacy-policy/) | No  | No ( [why not?](https://libredd.it/r/StartpageSearch/comments/lge9n0/is_startpage_open_source/gmtig3v/) ) |
|     | [Searx](https://searx.me/) | N/A 🌍 | N/A | [Yes](http://ulrn6sryqaifefld.onion/) | [GitHub](https://github.com/asciimoo/searx) |
|     | [Qwant](https://www.qwant.com/) | France 🇫🇷 | [Yes](https://about.qwant.com/legal/privacy/) | No  | [GitHub](https://github.com/Qwant/) |
|     | [Mojeek](https://www.mojeek.com/) | UK 🇬🇧 | [Yes](https://www.mojeek.com/about/privacy/) | No  | No  |
|     | [MetaGer](https://metager.org/) | Germany 🇩🇪 | [Yes](https://metager.org/datenschutz) | No  | [GitLab](https://gitlab.metager.de/open-source/MetaGer) |
|     | YaCy ( P2P ) | N/A 🌍 | N/A | N/A | [GitHub](https://github.com/yacy/yacy_search_server) |

<br><br>

**MORE RESOURCES:**

- [**HOW TO: Follow People on Twitter and Instagram Without an Account**](https://www.funkyspacemonkey.com/how-to-follow-people-on-twitter-and-instagram-without-an-account)
- [**HOW TO: Follow Reddit Subs Without an Account and Subscribe to Newsletters to Declutter Your Inbox**](https://www.funkyspacemonkey.com/how-to-follow-reddit-subs-without-an-account-and-subscribe-to-newsletters-to-declutter-your-inbox)
- [**HOW TO: Watch YouTube and Keep Your Subscriptions Without Ads and Google’s Tracking**](https://www.funkyspacemonkey.com/how-to-watch-youtube-and-keep-your-subscriptions-without-ads-and-googles-tracking)